#!/usr/bin/python

import re
import json
from json import JSONEncoder
import collections
import urllib
import urllib.request, urllib.request
from bs4 import BeautifulSoup
import argparse

class Group:
    child_type=None
    have_child=True
    alias={}

    def __init__(self, name="", code=0, children=set(), description=""):
        if code == 0:
            self.code = str(code)
            self.name = str(name)
        else:
            try:
                self.code, self.name = name.split("-")
            except ValueError:
                self.code = str(code)
                self.name = str(name)
        self.description = description;
        self.children = set()
        if self.have_child:
            self.add(children)

    def add(self, children):
        children={child for child in (children if isinstance(children, collections.Iterable) else {children}) if isinstance(child, self.child_type)}
        if self.have_child:
            if self.child_type.have_child:
                for child in children:
                        try:
                            next(x for x in self.children if x.code == child.code and x.name == child.name).add(child.children)
                        except StopIteration:
                            self.children.add(child)
            else:
                self.children.update(children)
        return self

    def sort(self):
        if self.have_child:
            for child in self.children:
                child.sort()
            self.children=set(sorted(children, key=lambda e: e.code))

    def load(self, json_dict):
        if not isinstance(json_dict, dict):
            return self
        if have_list(list(self.__dict__.keys()), list(json_dict.keys())):
            for key in self.__dict__.keys():
                if key is not "children":
                    self.__dict__[key] = json_dict[key]
            if self.have_child:
                for item in json_dict["children"]:
                    self.add(self.child_type().load(item))
        return self

    def find(self, func):
        res = []
        if callable(func):
            if func(self):
                res.append(self)
            elif self.have_child and issubclass(self.child_type, Group):
                for child in self.children: res += child.find(func)
        return res

    def __eq__(self, other):
        return hash(self) == hash(other)

    def __hash__(self):
        return hash((self.name, self.code, tuple(self.children)))

    def __getattr__(self, name):
        try:
            self.alias[name]
        except KeyError:
            raise NameError("No attr " + name)
        else:
            return getattr(self, self.alias[name])

class Spec(Group):
    have_child=False;
    def __init__(self, name="", code="", num="", money="", study_year="", description=""):
        super().__init__(name, code, description=description)
        self.num = num
        self.money = money
        self.study_year = study_year
    
class SpecGroup(Group):
    child_type=Spec
    alias={"specs": "children"}

class School(Group):
    child_type=SpecGroup
    alias={"spec_groups": "children"}

    def __init__(self, name="", location=""):
        super().__init__(name)
        self.location = location

class Batch(Group):
    child_type=School
    alias={"schools": "children"}

class Root(Group):
    child_type=Batch
    alias={"batchs": "children"}

class DictEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, set):
            return list(o)
        return o.__dict__

class ZyfzApi:
    def __init__(self, cookie, check_code=None):
        self.cookie = cookie
        if check_code:
            self.check_code = check_code
        else:
            self.check_code = 0
            self.post_zyfz(1, update_check_code=True)

    def post_zyfz(self, page, jhlx=5, update_check_code=False, debug=False):
        url = 'http://zyfz.shmeea.edu.cn/login/getJhkList.action'
        values = {"pageNumStrIng": str(page), "jhlx": str(jhlx), "area": "00", "showAll": "2", "checkCode": self.check_code}  
        headers = {'Cookie': self.cookie} #set the cookie here  
        data =  urllib.parse.urlencode(values).encode()  
        request = urllib.request.Request(url, data, headers)  
        soup = BeautifulSoup(urllib.request.urlopen(request).read().decode("UTF-8"))
        self.check_code=soup.select("#checkCode")[0].attrs["value"]
        if update_check_code: return self.check_code
        if debug: return soup
        try:
            batch=soup.select("#jhlx")[0].find_all(lambda tag: tag.has_attr("selected"))[0].text
        except IndexError:
            raise IndexError("batch index out of range")
        spgs=[dict(zip(["school", "spec_group", "request", "spec", "num", "money", "study_year", "location", "description"] , [t.text for t in e.find_all("label")[:-2]])) for e in soup.select("#jhkTable li")[1:]]
        if len(spgs) == 0: raise IndexError("page index out of range")
        return (batch, page, spgs, soup)

have_list = lambda l, tar: (len(l) == 1 or have_list(l[:-1], tar)) if l[-1] in tar else False

def post(zyfz, root):
    for batch in list(range(0, 6)) + [9]:
        for page in range(1,85):
            try:
                resp=zyfz.post_zyfz(page, batch)
            except IndexError:
                break
            print(batch, page)
            for spec_full in resp[2]:
                bth=Batch(resp[0], batch)
                sch=School(spec_full["school"], spec_full["location"])
                spg=SpecGroup(spec_full["spec_group"])
                spc=Spec(spec_full["spec"], 0, spec_full["num"], spec_full["money"], spec_full["study_year"], spec_full["description"])
                root.add(bth.add(sch.add(spg.add(spc))))
    return root

def jsonize(o):
    return json.dumps(o, cls=DictEncoder, ensure_ascii=False, indent=4)

# parse args
parse = argparse.ArgumentParser()
parse.add_argument("-c", "--cookie", help="set cookie", type=str)
parse.add_argument("-k", "--check-code", help="set check code", type=str)
parse.add_argument("-f", "--data-file", help="data", type=str)
args = parse.parse_args()

root = Root("志愿表")

if args.cookie is not None:
    zyfz = ZyfzApi(args.cookie, args.check_code)
if args.data_file is not None:
    with open(args.data_file) as data_file: root.load(json.load(data_file))
    def save(string):
        with open(args.data_file, 'wt') as f: print(string, file=f)
    
    